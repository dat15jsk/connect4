package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import io.HighscoreWriter;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import model.Board;
import view.GUI;

public class Controller implements EventHandler<Event> {

    private Board model;
    private GUI view;
    private ArrayList<String> history;

    /**
     * Creates a new controller and initiates the history list.
     */
    public Controller() {
        history = new ArrayList<String>();
    }

    /*
     * Handles all the different (relevant) events that can occur from user
     * input.
     * 
     * @see javafx.event.EventHandler#handle(javafx.event.Event)
     */
    @Override
    public void handle(Event event) {
        Object source = event.getSource();

        if (source.equals(view.getPlayButton())) {
            String player1 = view.getPlayer1().getText();
            String player2 = view.getPlayer2().getText();
            if (player1.equals(player2)) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Connect Four Dialog");
                alert.setHeaderText("Players have the same name.");
                alert.setContentText("Please use unequal player names.");
                alert.showAndWait();
            } else {
                view.startGame(this);
            }
        }

        if (source.equals(view.getNewGameButton())) {
            model.clear();
            history.clear();
            view.newGame(this);
        }

        if (source.equals(view.getSaveButton())) {
            if (!model.gameOver()) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Save Game");
                fileChooser.setInitialDirectory(new File("./"));
                fileChooser.setInitialFileName("MySave.txt");
                File file = fileChooser.showSaveDialog(view.getStage());
                if (file != null) {
                    try {
                        saveGame(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                view.displayPlain("Cannot save a completed game!");
            }
        }

        if (source.equals(view.getLoadButton())) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Load Game");
            fileChooser.setInitialDirectory(new File("./"));
            File file = fileChooser.showOpenDialog(view.getStage());
            if (file != null) {
                try {
                    loadGame(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (source.equals(view.getHighscoreButton())) {
            try {
                view.showHighscore();
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (source.equals(view.backButton())) {
            view.hideHighscore();
        }

        for (int row = 0; row < 6; row++) {
            for (int col = 0; col < 7; col++) {
                if (source.equals(view.getPreviews()[row][col]) && event.getEventType().equals(MouseEvent.MOUSE_CLICKED)
                        && !view.getTurnInProgress()) {
                    try {
                        makeMove(row, col);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (source.equals(view.getPreviews()[row][col])
                        && event.getEventType().equals(MouseEvent.MOUSE_ENTERED))
                    view.mouseEnterSlot(view.getPreviews()[row][col]);
                if (source.equals(view.getPreviews()[row][col]) && event.getEventType().equals(MouseEvent.MOUSE_EXITED))
                    view.mouseExitSlot(view.getPreviews()[row][col]);
            }
        }

    }

    /**
     * Sets the specified slot in the model and adds the move to the history
     * list. If a player wins, it's highscore is incremented.
     * 
     * @param row
     *            - The row of which the move is placed
     * @param col
     *            - The column of which the move is placed
     * @throws NumberFormatException
     * @throws IOException
     */
    private void makeMove(int row, int col) throws NumberFormatException, IOException {
        String player = view.currentPlayerName();
        String color = view.currentColorToString();
        history.add(player + "; " + color + "; " + row + "; " + col);
        model.setSlot(row, col);
        if (model.gameOver()) {
            if (model.isDraw()) {
                history.add("Game Result: Draw");
            } else {
                history.add("Game Result: " + player + " won");
                HighscoreWriter.add(player);
            }
        }
    }

    /**
     * Saves the current state of the game into a text file.
     * 
     * @param file
     *            - The file to be written to.
     * @throws IOException
     */
    private void saveGame(File file) throws IOException {
        FileWriter writer = new FileWriter(file);
        writer.write(view.getPlayer1().getText() + "; " + view.getPlayer2().getText() + "\n");
        for (String s : history) {
            writer.write(s);
            writer.write("\n");
        }
        writer.close();

    }

    /**
     * Loads a previously saved game state from a text file.
     * 
     * @param file
     *            - The save file.
     * @throws IOException
     */
    private void loadGame(File file) throws IOException {
        view.newGame(this);
        model.clear();
        history.clear();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String[] players = reader.readLine().split("; ");
        view.setPlayers(players[0], players[1]);
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.contains("; ")) {
                String[] split = line.split("; ");
                makeMove(Integer.parseInt(split[2]), Integer.parseInt(split[3]));
            }
        }
        reader.close();
    }

    /**
     * Sets the model that this controller is in controll of.
     * 
     * @param board
     *            - Board to be controlled.
     */
    public void setModel(Board board) {
        this.model = board;
    }

    /**
     * Sets the view that this controller is in controll of.
     * 
     * @param gui
     *            - The GUI to be controlled.
     */
    public void setView(GUI gui) {
        this.view = gui;
    }
}
