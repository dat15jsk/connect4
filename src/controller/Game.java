package controller;

import javafx.application.Application;
import javafx.stage.Stage;
import model.Board;
import view.GUI;

public class Game extends Application {

    private GUI view;
    private Controller controller;

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * The application subclass. Initiates a model, view and controller for the game.
     */
    public Game() {
        Board model = new Board();
        view = new GUI();
        model.addObserver(view);
        controller = new Controller();
        controller.setModel(model);
        controller.setView(view);
    }

    @Override
    public void start(Stage stage) throws Exception {
        view.setUp(stage, controller);
    }

}
