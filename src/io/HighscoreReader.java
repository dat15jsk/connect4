package io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class HighscoreReader {
    
    /**Reads the highscore from highscore.txt
     * @return - A TreeMap containing the highscores.
     * @throws NumberFormatException
     * @throws IOException
     */
    public static TreeMap<String, Integer> readScore() throws NumberFormatException, IOException {
        TreeMap<String, Integer> scores = new TreeMap<String, Integer>();
        File scoreFile = new File("./" + "highscore.txt");
        scoreFile.createNewFile();
        BufferedReader reader = new BufferedReader(new FileReader(scoreFile));
        String line;
        while((line = reader.readLine()) != null) {
            if (line.contains("; ")) {
                String[] split = line.split("; ");
                scores.put(split[0], Integer.parseInt(split[1]));
            }
        }
        reader.close();
        return scores;
    }
    
    /**Creates a TreeMap of readScore() with switched key and value in the entry set.
     * This is so the natural order of the Map is sorted on the highscore instead of the name.
     * @return - A TreeMap containing the highscores, sorted by score
     * @throws NumberFormatException
     * @throws IOException
     */
    public static TreeMap<Integer, String> createList() throws NumberFormatException, IOException {
        TreeMap<String, Integer> list = readScore();
        TreeMap<Integer, String> scores = new TreeMap<Integer, String>();
        for(Map.Entry<String, Integer> e: list.entrySet()) {
            scores.put(e.getValue(), e.getKey());
        }
        return scores;
    }
 
}
