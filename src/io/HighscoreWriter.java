package io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class HighscoreWriter {
    
    /** Adds a player to the highscore file. It utilizes the readScore() function of HighscoreReader,
     * instead of createList() since readScore() returns a list which prevents duplicate players.
     * @param player - The player to be added.
     * @throws NumberFormatException
     * @throws IOException
     */
    public static void add(String player) throws NumberFormatException, IOException {
        TreeMap<String, Integer> scoreList = HighscoreReader.readScore();
        if(scoreList.containsKey(player)) {
            scoreList.put(player, scoreList.get(player) + 1);
        } else {
            scoreList.put(player, 1);
        }
        FileWriter writer = new FileWriter(new File("./" + "highscore.txt"));
        
        for(Map.Entry<String, Integer> e: scoreList.entrySet()) {
            writer.write(e.getKey() + "; " + Integer.toString(e.getValue()) + "\n");
        }
        writer.close();
    }

}
