package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

import javafx.scene.paint.Color;

public class Board extends Observable {

    private Slot[][] slots;
    private boolean gameOver;
    private ArrayList<Slot> solution;
    private Color currentPlayer;

    /**
     * Creates a new board and initializes the fields.
     */
    public Board() {
        slots = new Slot[6][7];
        gameOver = false;
        solution = new ArrayList<Slot>();
        currentPlayer = Color.RED;
        for (int row = 0; row < 6; row++) {
            for (int col = 0; col < 7; col++) {
                slots[row][col] = new Slot(row, col, Color.TRANSPARENT);
            }
        }
    }

    /** 
     * @param row - Row of the slot
     * @param col - Column of the slot
     * @return - The slot on specified row and column.
     */
    public Slot getSlot(int row, int col) {
        return slots[row][col];
    }

    /** Sets the specified slot and notifies observers (view) of the change. Checks if a solution has been found.
     * @param row - Row of the slot
     * @param col - Column of the slot
     */
    public void setSlot(int row, int col) {
        if (!gameOver) {
            slots[row][col].setPlayer(currentPlayer);
            changeCurrentColor();
            setChanged();
            notifyObservers(slots[row][col]);
            updateNeighbors();
            checkSolution();
        }
    }

    /**
     * Clears the board from slots.
     */
    public void clear() {
        gameOver = false;
        solution = new ArrayList<Slot>();
        currentPlayer = Color.RED;
        for (int row = 0; row < 6; row++) {
            for (int col = 0; col < 7; col++) {
                slots[row][col] = new Slot(row, col, Color.TRANSPARENT);
            }
        }
    }

    /**
     * @return - A list containing the slots that are part of the solution.
     */
    public ArrayList<Slot> getSolution() {
        return solution;
    }

    /**
     * @return - The current color to be placed next.
     */
    public Color getCurrentColor() {
        return currentPlayer;
    }

    /**
     *  Changes the current color to the other player's.
     */
    private void changeCurrentColor() {

        if (currentPlayer.equals(Color.RED)) {
            currentPlayer = Color.YELLOW;
        } else {
            currentPlayer = Color.RED;
        }
    }

    /**
     * @return - If the game is over.
     */
    public boolean gameOver() {
        return gameOver;
    }

    /**
     * @return - If the game is a draw.
     */
    public boolean isDraw() {
        return solution.size() == 0 && gameOver;
    }

    /**
     *  Checks all slots if a solution is present.
     */
    public void checkSolution() {
        int count = 0;
        for (int row = 0; row < 6; row++) {
            for (int col = 0; col < 7; col++) {
                checkSpecificSlot(row, col);
                if (!slots[row][col].getPlayer().equals(Color.TRANSPARENT))
                    count++;
            }
        }
        if (count == 42) {
            gameOver = true;
            Boolean arg = isDraw();
            setChanged();
            notifyObservers(arg);
        }
    }

    /**Checks a specific slot if any of it's neighboring slots are too part of an eventual solution.
     * @param row - Row of the specified slot.
     * @param col - Column of the specified slot.
     */
    private void checkSpecificSlot(int row, int col) {
        Slot slot = slots[row][col];
        for (Integer i : slot.getNeighbors().keySet()) {
            ArrayList<Slot> connectedSlots = new ArrayList<Slot>();
            checkSpecificNeighbor(slot, i, connectedSlots);
        }
    }

    /** Recursive algorithm to determine if a specific neighbor has a neighbor that has a neighbor which all are the same color
     * as the original slot. If so, a solution has been found and the view is notified.
     * @param slot - The slot to check.
     * @param key - Which neighbor to check.
     * @param connectedSlots - List to keep track of how many connectedSlots has been found.
     */
    private void checkSpecificNeighbor(Slot slot, int key, ArrayList<Slot> connectedSlots) {
        connectedSlots.add(slot);
        if (connectedSlots.size() == 4 && !gameOver) {
            solution.addAll(connectedSlots);
            gameOver = true;
            Boolean arg = isDraw();
            setChanged();
            notifyObservers(arg);
        } else {
            HashMap<Integer, Slot> neighbors = slot.getNeighbors();
            if (neighbors.containsKey(key)) {
                Color color = neighbors.get(key).getPlayer();
                if (color.equals(slot.getPlayer()) && !color.equals(Color.TRANSPARENT)) {
                    checkSpecificNeighbor(neighbors.get(key), key, connectedSlots);
                } else {
                    connectedSlots.clear();
                }
            } else {
                connectedSlots.clear();
            }
        }
    }

    /**
     *  Updates neighbors for each and every slot.
     */
    private void updateNeighbors() {
        for (int row = 0; row < 6; row++) {
            for (int col = 0; col < 7; col++) {
                if (row - 1 >= 0 && col - 1 >= 0)
                    slots[row][col].addNeighbor(1, slots[row - 1][col - 1]);
                if (row - 1 >= 0)
                    slots[row][col].addNeighbor(2, slots[row - 1][col]);
                if (row - 1 >= 0 && col + 1 < 7)
                    slots[row][col].addNeighbor(3, slots[row - 1][col + 1]);
                if (col - 1 >= 0)
                    slots[row][col].addNeighbor(4, slots[row][col - 1]);
                if (col + 1 < 7)
                    slots[row][col].addNeighbor(6, slots[row][col + 1]);
                if (row + 1 < 6 && col - 1 >= 0)
                    slots[row][col].addNeighbor(7, slots[row + 1][col - 1]);
                if (row + 1 < 6)
                    slots[row][col].addNeighbor(8, slots[row + 1][col]);
                if (row + 1 < 6 && col + 1 < 7)
                    slots[row][col].addNeighbor(9, slots[row + 1][col + 1]);
            }
        }
    }
}
