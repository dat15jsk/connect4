package model;

import java.util.HashMap;

import javafx.scene.paint.Color;

public class Slot {
       
    private int row;
    private int col;
    private Color player;
    private HashMap<Integer,Slot> neighbors;
    
    /** Creates a single slot.
     * @param row - Row of the slot.
     * @param col - Column of the slot.
     * @param player - Which player the slot belongs to.
     */
    public Slot(int row, int col, Color player) {
        this.row = row;
        this.col = col;
        this.player = player;
        neighbors = new HashMap<Integer, Slot>();
    }
    
    /**
     * @return - The row of the slot.
     */
    public int getRow() {
        return row;
    }
    
    /**
     * @return - The column of the slot.
     */
    public int getCol() {
        return col;
    }
    
    /**
     * @return - The color of the slot.
     */
    public Color getPlayer() {
        return player;
    }
    
    /** Sets the color the that of a player.
     * @param player - The player who's color this slot should be set to.
     */
    public void setPlayer(Color player) {
        this.player = player;
    }
    
    /**
     * @return - The neighbors for this slot.
     */
    public HashMap<Integer, Slot> getNeighbors() {
        return neighbors;
    }
    
    /**Adds a neighbor to a certain key. The key of a neighbor is determined by picturing a phone number pad: 
     * 1,2,3 are the top neighbors, 4 & 6 are left and right neighbors and 7,8,9 are the bottom neighbors.
     * @param key - Adds a neighbor to a certain key
     * @param neighbor
     */
    public void addNeighbor(int key, Slot neighbor) {
        neighbors.put(key, neighbor);
    }
}
