package view;

import controller.Controller;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public class CellFactory {

    /** A factory which creates cells for the grid which will be the playing field and sets eventhandlers for each cell.
     * @param rows - Row of the cell.
     * @param cols - Column of the cell.
     * @param disks - The matrix of disks of the view.
     * @param diskPreviews - The matrix of diskPreviews of the matrix.
     * @param controller - The controller.
     * @return - A stackpane containing the cell.
     */
    public static StackPane create(int rows, int cols, Circle[][] disks, Circle[][] diskPreviews,
            Controller controller) {
        Rectangle rect = new Rectangle(50, 50);
        Circle circ = new Circle(20);
        circ.centerXProperty().set(25);
        circ.centerYProperty().set(25);
        Shape cell = Path.subtract(rect, circ);
        cell.setFill(Color.BLUE);
        Circle diskPreview = new Circle(20);
        diskPreviews[rows][cols] = diskPreview;
        diskPreview.setFill(Color.TRANSPARENT);
        diskPreview.setOpacity(0.5);
        diskPreview.addEventHandler(MouseEvent.MOUSE_CLICKED, controller);
        diskPreview.addEventHandler(MouseEvent.MOUSE_ENTERED, controller);
        diskPreview.addEventHandler(MouseEvent.MOUSE_EXITED, controller);
        Circle disk = new Circle(20);
        disks[rows][cols] = disk;
        disk.setFill(Color.TRANSPARENT);
        disk.setTranslateY(-(50 * (rows + 1)));
        StackPane stack = new StackPane();
        stack.getChildren().addAll(cell, diskPreview, disk);
        return stack;
    }
}
