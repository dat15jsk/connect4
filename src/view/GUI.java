package view;

import java.io.IOException;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeMap;

import controller.Controller;
import io.HighscoreReader;
import javafx.animation.AnimationTimer;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Board;
import model.Slot;

public class GUI implements Observer {

    private final static double MIN_WIDTH = 960.0;
    private final static double MIN_HEIGHT = 540.0;

    private Stage stage;
    private Scene scene;
    private GridPane gridpane;
    private BorderPane root;
    private boolean turnInProgress = false;
    private boolean gameOver = false;
    private Color currentColor = Color.RED;
    private Circle[][] disks = new Circle[6][7];
    private Circle[][] diskPreviews = new Circle[6][7];
    private Button play = new Button("Play!");
    private TextField playerName1;
    private TextField playerName2;
    private Button newGame = new Button("New Game");
    private Button save = new Button("Save game");
    private Button load = new Button("Load game");
    private Button highscore = new Button("Highscore");
    private Button back = new Button("Back");
    private Text status = new Text();
    private HBox buttons = new HBox(20);
    private VBox statusWrapper = new VBox();

    public Stage getStage() {
        return stage;
    }

    public TextField getPlayer1() {
        return playerName1;
    }

    public TextField getPlayer2() {
        return playerName2;
    }

    public boolean getTurnInProgress() {
        return turnInProgress;
    }

    public Button getPlayButton() {
        return play;
    }

    public Button getNewGameButton() {
        return newGame;
    }

    public Button getSaveButton() {
        return save;
    }

    public Button getLoadButton() {
        return load;
    }

    public Button getHighscoreButton() {
        return highscore;
    }

    public Button backButton() {
        return back;
    }

    public Circle[][] getDisks() {
        return disks;
    }

    public Circle[][] getPreviews() {
        return diskPreviews;
    }

    public String currentColorToString() {
        if (currentColor.equals(Color.RED)) {
            return "RED";
        } else {
            return "YELLOW";
        }
    }

    public void setPlayers(String player1, String player2) {
        playerName1.setText(player1);
        playerName2.setText(player2);
    }

    public String currentPlayerName() {
        String player;
        if (currentColor.equals(Color.RED)) {
            player = playerName1.getText();
        } else {
            player = playerName2.getText();
        }
        return player;
    }

    public void setUp(Stage stage, Controller controller) {
        this.stage = stage;
        stage.setTitle("Connect 4");
        stage.getIcons().add(new Image("file:icon.png"));
        stage.setResizable(false);
        stage.setMinWidth(MIN_WIDTH);
        stage.setMinHeight(MIN_HEIGHT);
        root = setUpMenu(controller);
        scene = new Scene(root, Color.ALICEBLUE);
        stage.setScene(scene);
        stage.show();
    }

    private BorderPane setUpMenu(Controller controller) {
        BorderPane root = new BorderPane();
        GridPane menu = new GridPane();
        Text player1 = new Text("Player 1");
        player1.setFont(new Font(50));
        player1.setStroke(Color.BLACK);
        player1.setFill(Color.RED);
        Text player2 = new Text("Player 2");
        player2.setFont(new Font(50));
        player2.setStroke(Color.BLACK);
        player2.setFill(Color.YELLOW);
        playerName1 = new TextField("David");
        playerName1.setFont(new Font(30));
        playerName1.setAlignment(Pos.CENTER);
        playerName2 = new TextField("Goliath");
        playerName2.setFont(new Font(30));
        playerName2.setAlignment(Pos.CENTER);
        play.setPrefSize(100, 50);
        play.setFont(new Font(20));
        play.addEventHandler(MouseEvent.MOUSE_CLICKED, controller);
        menu.add(player1, 0, 0);
        menu.add(player2, 1, 0);
        menu.add(playerName1, 0, 1);
        menu.add(playerName2, 1, 1);
        menu.add(play, 1, 1);
        GridPane.setConstraints(player1, 0, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER,
                new Insets(10));
        GridPane.setConstraints(player2, 1, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER,
                new Insets(10));
        GridPane.setConstraints(playerName1, 0, 1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER,
                new Insets(10));
        GridPane.setConstraints(playerName2, 1, 1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER,
                new Insets(10));
        GridPane.setConstraints(play, 0, 2, 2, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER,
                new Insets(10));
        menu.setAlignment(Pos.TOP_CENTER);
        root.setCenter(menu);
        VBox titleBox = new VBox();
        Image title = new Image("file:connect4logo.png");
        titleBox.getChildren().add(new ImageView(title));
        titleBox.setAlignment(Pos.CENTER);
        root.setTop(titleBox);
        root.setBackground(null);
        return root;
    }

    public void startGame(Controller controller) {
        stage.maximizedProperty().addListener(evt -> scaleChildren());
        stage.widthProperty().addListener(evt -> scaleChildren());
        stage.heightProperty().addListener(evt -> scaleChildren());
        stage.setResizable(true);
        newGame(controller);
    }

    public void newGame(Controller controller) {
        root.getChildren().clear();
        gridpane = createGrids(controller);
        gridpane.setAlignment(Pos.CENTER);
        root.setCenter(gridpane);
        newGame.addEventHandler(MouseEvent.MOUSE_CLICKED, controller);
        save.addEventHandler(MouseEvent.MOUSE_CLICKED, controller);
        load.addEventHandler(MouseEvent.MOUSE_CLICKED, controller);
        highscore.addEventHandler(MouseEvent.MOUSE_CLICKED, controller);
        back.addEventHandler(MouseEvent.MOUSE_CLICKED, controller);
        newGame.setFont(new Font(20));
        save.setFont(new Font(20));
        load.setFont(new Font(20));
        highscore.setFont(new Font(20));
        if (buttons.getChildren().isEmpty())
            buttons.getChildren().addAll(newGame, save, load, highscore);
        buttons.setAlignment(Pos.CENTER);
        buttons.setPadding(new Insets(20));
        root.setBottom(buttons);
        status.setFont(new Font(40));
        status.setTextAlignment(TextAlignment.CENTER);
        if (statusWrapper.getChildren().isEmpty())
            statusWrapper.getChildren().add(status);
        statusWrapper.setAlignment(Pos.CENTER);
        root.setTop(statusWrapper);
        gameOver = false;
        currentColor = Color.RED;
        scaleChildren();
        displayTurn();
    }

    private GridPane createGrids(Controller controller) {
        GridPane gridpane = new GridPane();
        gridpane.getChildren().clear();
        for (int rows = 0; rows < 6; rows++) {
            for (int cols = 0; cols < 7; cols++) {
                StackPane stack = CellFactory.create(rows, cols, disks, diskPreviews, controller);
                gridpane.add(stack, cols, rows);
            }
        }
        return gridpane;
    }

    // Not as horrific as it may sound
    private void scaleChildren() {
        double width = stage.getWidth();
        double height = stage.getHeight();
        double scale;
        if (width / height >= MIN_WIDTH / MIN_HEIGHT) {
            scale = height / MIN_HEIGHT;
        } else {
            scale = width / MIN_WIDTH;
        }
        newGame.setFont(new Font(20 * scale));
        save.setFont(new Font(20 * scale));
        load.setFont(new Font(20 * scale));
        highscore.setFont(new Font(20 * scale));
        gridpane.setScaleX(scale);
        gridpane.setScaleY(scale);
        status.setFont(new Font(40 * scale));
    }

    private void setDisk(int row, int col, int duration) {
        Circle disk = disks[row][col];
        TranslateTransition translateTransition = new TranslateTransition(Duration.millis(duration), disk);
        translateTransition.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                turnInProgress = false;
            }
        });
        if (disk.getTranslateY() != 0 && !gameOver) {
            turnInProgress = true;
            translateTransition.setToY(0);
            disk.setFill(currentColor);
            translateTransition.play();
        }
    }

    public void mouseEnterSlot(Circle diskPreview) {
        if (!gameOver) {
            diskPreview.setFill(currentColor);
        }
    }

    public void mouseExitSlot(Circle diskPreview) {
        diskPreview.setFill(Color.TRANSPARENT);
    }

    public void showHighscore() throws NumberFormatException, IOException {
        GridPane table = new GridPane();
        TreeMap<Integer, String> scoreList = HighscoreReader.createList();
        int rowCount = scoreList.entrySet().size();
        for (Map.Entry<Integer, String> e: scoreList.entrySet()) {
            Text name = new Text(e.getValue());
            name.setFont(new Font(20));
            GridPane.setConstraints(name, 0, rowCount, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER,
                    new Insets(10));
            Text score = new Text(Integer.toString(e.getKey()));
            score.setFont(new Font(20));
            GridPane.setConstraints(score, 1, rowCount, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER,
                    new Insets(10));
            table.add(name, 0, rowCount);
            table.add(score, 1, rowCount);
            rowCount--;
        }
        table.setPadding(new Insets(10));
        table.setAlignment(Pos.CENTER);
        root.setCenter(table);
        VBox backButtonWrapper = new VBox();
        backButtonWrapper.getChildren().add(back);
        back.setAlignment(Pos.CENTER);
        back.setFont(new Font(20));
        backButtonWrapper.setAlignment(Pos.CENTER);
        backButtonWrapper.setPadding(new Insets(20));
        root.setBottom(backButtonWrapper);
        root.setTop(null);
    }

    public void hideHighscore() {
        root.setTop(statusWrapper);
        root.setCenter(gridpane);
        root.setBottom(buttons);
    }

    private void displayTurn() {
        String player = currentPlayerName();
        status.setText(player + "'s turn");
        status.setFill(currentColor);
        status.setStroke(Color.BLACK);
    }

    private void displayWinner(Board board) {
        Color winner = board.getSolution().get(0).getPlayer();
        if (winner.equals(Color.RED)) {
            status.setText(playerName1.getText() + " has won!");
        } else {
            status.setText(playerName2.getText() + " has won!");
        }
        status.setFill(winner);
    }

    public void displayPlain(String message) {
        status.setFill(Color.BLACK);
        status.setText(message);
    }

    private void displaySolution(Board board) {
        for (Slot s : board.getSolution()) {
            new AnimationTimer() {
                @Override
                public void handle(long now) {
                    if (now % 600000000 > 300000000) {
                        if (s.getPlayer().equals(Color.RED))
                            disks[s.getRow()][s.getCol()].setFill(Color.LIGHTCORAL);
                        if (s.getPlayer().equals(Color.YELLOW))
                            disks[s.getRow()][s.getCol()].setFill(Color.CYAN);
                    } else {
                        disks[s.getRow()][s.getCol()].setFill(s.getPlayer());
                    }
                    if (!board.gameOver()) {
                        disks[s.getRow()][s.getCol()].setFill(board.getSlot(s.getRow(), s.getCol()).getPlayer());
                        stop();
                    }
                }
            }.start();
        }
    }

    private void gameWon(Board board) {
        displayWinner(board);
        displaySolution(board);
    }

    @Override
    public void update(Observable o, Object arg) {
        Board board = (Board) o;
        if (arg instanceof Slot) {
            Slot slot = (Slot) arg;
            setDisk(slot.getRow(), slot.getCol(), 200);
            currentColor = board.getCurrentColor();
            displayTurn();
        }
        if (arg instanceof Boolean) {
            gameOver = board.gameOver();
            if (!(boolean) arg) {
                gameWon(board);
            } else {
                displayPlain("It's a draw!");
            }
        }
    }
}